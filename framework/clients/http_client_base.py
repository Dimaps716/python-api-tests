import json
from urllib.parse import urljoin

import requests


class RequestType:
    GET = "GET"
    POST = "POST"
    PUT = "PUT"
    PATCH = "PATCH"
    DELETE = "DELETE"


class HttpClientBase:
    def __init__(self, base_url):
        self.base_url = base_url
        self.__request = requests.request

    @staticmethod
    def _prettify_json(json_str):
        try:
            parsed_json = json.loads(json_str)
        except json.JSONDecodeError:
            return json_str

        return json.dumps(parsed_json, indent=4, sort_keys=True, ensure_ascii=False)

    def request(self, method, uri, params=None, json_type=None, files=None, data=None,
                headers=None, timeout=None, verify=True):
        """Handle http request

        :param uri: URI.
        :param method: http method.
            :class:RequestType
        :param params: (optional) dictionary with params to send.
        :param json_type: (optional) json to send in the request body.
        :param files: (optional) Dictionary of ``'form-parameter-name': file-like-objects``
            for multipart encoding upload.
        :param data: (optional) Dictionary, bytes, or file-like object to send
            in the body of the :class:requests.Request.
        :param headers: (optional) Dictionary of HTTP Headers to send with the
            :class:requests.Request.
        :param timeout: (optional) How long to wait for the server to send
            data before giving up, as a float, or a :ref:`(connect timeout,
            read timeout) <timeouts>` tuple.
        :param verify: (optional) Either a boolean, in which case it controls whether we verify
            the server's TLS certificate, or a string, in which case it must be a path
            to a CA bundle to use. Defaults to ``True``.
        :returns: instance of :class:requests.Response.
        """
        url = urljoin(self.base_url, uri)

        response = self.__request(
            url=url, method=method, params=params, json=json_type, files=files, data=data,
            headers=headers, timeout=timeout, verify=verify)

        return response
