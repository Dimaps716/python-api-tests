# Тесты на api method regions

### Установка зависимостей

Для установки зависимостей выполняем следующие команды:
```bash
$ pip install tox
```
Эта команда подготовит окружение.
```bash
$ tox
```
Для обновления окружения выполните:
```bash
tox --recreate
```

### Запуск тестов
Запуск всех тестов производится командой:
```bash
tox -e tests
```
При необходимости можно передать дополнительные аргуметы в py.test и/или указать точный путь до тестов
```bash
tox -e tests --maxfail=2 tests/regions/test_regions.py
```

### Создание отчета в allure
```bash
allure serve $(pwd)/allure-report
```