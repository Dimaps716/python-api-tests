import pytest
from requests import HTTPError

from framework.constants.responses import Responses
from framework.entities.response_entities import QueryInfo, QueryError, ItemInfo, CountryInfo, QueryErrorMessage


def get_successful_request_model(api_client, query=None, country_code=None, page=None, page_size=None):
    response = api_client.get_cities_by_query_params(
        q=query, country_code=country_code, page=page, page_size=page_size
    )
    if len(response['items']) == 0:
        successful_query_model = QueryInfo(
            total=response['total'],
            items=[]
        )
    else:
        successful_query_model = QueryInfo(
            total=response['total'],
            items=[
                ItemInfo(
                    id=item['id'],
                    name=item['name'],
                    code=item['code'],
                    country=CountryInfo(
                        name=item['country']['name'],
                        code=item['country']['code']
                    ),
                ) for item in response['items']
            ]
        )
    return successful_query_model


def get_error_request_model(api_client, query=None, country_code=None, page=None, page_size=None):
    response = api_client.get_cities_by_query_params(
        q=query, country_code=country_code, page=page, page_size=page_size
    )
    error_query_model = QueryError(
        error=QueryErrorMessage(
            id=response['error']['id'],
            message=response['error']['message']
        )
    )
    return error_query_model


def test_less_than_3_characters_in_search(api_client):
    query_model = get_error_request_model(api_client, 'но')
    assert Responses.more_than_3_characters == query_model.error.message


def test_search_empty(api_client):
    query_model = get_successful_request_model(api_client, query='')
    assert len(query_model.items) != 0


@pytest.mark.parametrize(
    argnames="query",
    argvalues=[
        "НОВОСИБИРСК",
        "новосибирск",
        "НОВОсибирск",
        "НОВОсибИРСК"
    ])
def test_case_does_not_matter(api_client, query):
    query_model = get_successful_request_model(api_client, query)
    assert 1 == len(query_model.items)


def test_end_search(api_client):
    query_model = get_successful_request_model(api_client, 'РСК')
    model_items = query_model.items
    assert 5 == len(model_items)
    for model in model_items:
        assert 'рск' in model.name


def test_eng_name_search(api_client):
    query_model = get_successful_request_model(api_client, 'novosibirsk')
    assert 0 == len(query_model.items)


@pytest.mark.parametrize(
    argnames="query",
    argvalues=[
        "новосбирск",
        "новосебирск",
        "Масква",
        "Моква"
    ])
def test_search_by_name_with_an_error(api_client, query):
    query_model = get_successful_request_model(api_client, query)
    assert 0 == len(query_model.items)


@pytest.mark.parametrize(
    argnames="query",
    argvalues=[
        "10.0",
        "100",
        "999999999"
    ])
def test_numeric_search(api_client, query):
    query_model = get_error_request_model(api_client, query)
    assert Responses.parameter_must_be_string_value == query_model.error.message


@pytest.mark.parametrize(
    argnames="query",
    argvalues=[
        "ново$ибирск",
        "Москв@",
        "Магн!тогорск",
        "Владивосток#%^",
        "Влад&кавка*"
    ])
def test_search_with_special_characters(api_client, query):
    query_model = get_successful_request_model(api_client, query)
    assert 0 == len(query_model.items)


def test_search_query_from_two_cities(api_client):
    query_model = get_successful_request_model(api_client, "МоскваНовосибирск")
    assert 0 == len(query_model.items)


def test_search_with_ignoring_parameters(api_client):
    query_model = get_successful_request_model(api_client, query='рск', country_code='ru')
    assert 5 == len(query_model.items)


def test_empty_parameters_except_q(api_client):
    query_model = get_error_request_model(api_client, query='', country_code='', page='', page_size='')
    assert Responses.more_than_3_characters == query_model.error.message
