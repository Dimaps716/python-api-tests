import pytest

from framework.clients.api_client import ApiClient
from framework.configs import config


@pytest.fixture(scope="session", name="api_client")
def get_api_client_fixture():
    return ApiClient(config.api.url)



