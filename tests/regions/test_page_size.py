import math

import pytest
from testfixtures import compare

from framework.constants.responses import Responses
from tests.regions.test_query import get_error_request_model, get_successful_request_model


def test_page_size_is_empty(api_client):
    query_model = get_error_request_model(api_client, page_size='')
    assert Responses.page_size_parameter_must_be_an_integer == query_model.error.message


def test_page_size_default_15(api_client):
    query_model_default = get_successful_request_model(api_client)
    query_model_page_size_15 = get_successful_request_model(api_client, page_size=15)
    compare(query_model_default, query_model_page_size_15)


@pytest.mark.parametrize(
    argnames="default_number",
    argvalues=[
        5,
        10,
        15,
    ])
def test_page_size_is_list(api_client, default_number):
    query_model = get_successful_request_model(api_client, page_size=default_number)
    assert default_number == len(query_model.items)


@pytest.mark.parametrize(
    argnames="default_number",
    argvalues=[
        0,
        1,
        6,
        12,
    ])
def test_page_size_should_be_from_the_list(api_client, default_number):
    query_model = get_error_request_model(api_client, page_size=default_number)
    assert Responses.page_size_parameter_should_be_from_the_list == query_model.error.message


@pytest.mark.parametrize(
    argnames="number",
    argvalues=[
        "f",
        "б"
        "1.0",
        "2.2",
        "3.00",
        "100,0"
    ])
def test_page_size_not_an_integer(api_client, number):
    query_model = get_error_request_model(api_client, page_size=number)
    assert Responses.page_size_parameter_must_be_an_integer == query_model.error.message


@pytest.mark.parametrize(
    argnames="special_characters",
    argvalues=[
        "@",
        "!",
        "$",
        "%",
        "^",
        "&",
        "*",
        "!@#$%^&*"
    ])
def test_page_size_special_characters(api_client, special_characters):
    query_model = get_error_request_model(api_client, page_size=special_characters)
    assert Responses.page_size_parameter_must_be_an_integer == query_model.error.message


@pytest.mark.parametrize(
    argnames="page_size",
    argvalues=[
        5,
        10,
        15
    ])
def test_total_number_of_items(api_client, page_size):
    total_count = api_client.get_cities_by_query_params()['total']
    page_count = math.ceil(total_count / page_size) + 1
    count = 0
    for i in range(1, page_count):
        query_model = get_successful_request_model(api_client, page=i, page_size=page_size)
        count += len(query_model.items)

    assert count == total_count
