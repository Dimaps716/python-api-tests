import pytest
from testfixtures import compare

from framework.constants.responses import Responses
from tests.regions.test_query import get_error_request_model, get_successful_request_model


def test_serial_number_is_empty(api_client):
    query_model = get_error_request_model(api_client, page='')
    assert Responses.page_parameter_must_be_an_integer == query_model.error.message


def test_serial_number_cannot_be_zero(api_client):
    query_model = get_error_request_model(api_client, page=0)
    assert Responses.page_parameter_should_not_be_0 == query_model.error.message


def test_default_serial_number_1(api_client):
    query_model_default = get_successful_request_model(api_client)
    query_model_page_size_15 = get_successful_request_model(api_client, page=1)
    compare(query_model_default, query_model_page_size_15)


@pytest.mark.parametrize(
    argnames="number",
    argvalues=[
        "f",
        "1.0",
        "2.2",
        "3.00",
        "100,0"
    ])
def test_serial_number_is_not_an_integer(api_client, number):
    query_model = get_error_request_model(api_client, page=number)
    assert Responses.page_parameter_must_be_an_integer == query_model.error.message


@pytest.mark.parametrize(
    argnames="special_characters",
    argvalues=[
        "@",
        "!",
        "$",
        "%",
        "^",
        "&",
        "*",
        "!@#$%^&*"
    ])
def test_serial_number_special_characters(api_client, special_characters):
    query_model = get_error_request_model(api_client, page=special_characters)
    assert Responses.page_parameter_must_be_an_integer == query_model.error.message
